// @flow
import { describe, it } from 'mocha';
import {
  assertThat, hasProperty, hasItem, hasItems, equalTo, contains, containsInAnyOrder, hasProperties,
} from 'hamjest';
import { HotelBuilder, RegionBuilder, CityBuilder } from './builders';
import { findDestination } from "./search";

describe('Find destinations', () => {
  describe('WHEN the `name` matches (partly)', () => {
    it('of a hotel', () => {
      const hotel = new HotelBuilder().addName('Hotel Neptuno Valencia').build();
      const destinations = findDestination([hotel], 'Hotel Neptuno');
      assertThat(destinations, contains(hotel));
    });
    it('of a city', () => {
      const city = new CityBuilder().addName('Valencia').build();
      const destinations = findDestination([city], 'Valencia');
      assertThat(destinations, contains(hasProperty('name', 'Valencia')));
    });
    it('of a region, city and hotel', () => {
      const allDestinations = [
        new HotelBuilder().addName('Hotel Neptuno Valencia').build(),
        new CityBuilder().addName('Valencia').build(),
        new RegionBuilder().addName('Valencia').build(),
      ];
      const destinations = findDestination(allDestinations, 'Valencia');
      assertThat(destinations, hasProperty('length', 3));
    });
  });

  describe('WHEN a `passion` matches (partly)', () => {
    it('of a hotel', () => {
      const allDestinations = [
        new HotelBuilder().addName('Some').addPassion('cocktails').build(),
        new HotelBuilder().addName('Other').addPassion('pool').build(),
        new HotelBuilder().addName('any').addPassion('beach').addPassion('cocktails').build(),
      ];
      const destinations = findDestination(allDestinations, 'cocktails');
      assertThat(destinations, hasProperty('length', 2));
    });
  });

  describe('WHEN multiple attributes match', () => {
    it('a `passion` and the `name` of a hotel', () => {
      const passion = 'cocktails';
      const cityAndPassionMatch = new HotelBuilder().addName('Some').addCity('Darmstadt').addPassion(passion).build();
      const onlyPassionMatches = new HotelBuilder().addName('any').addPassion('beach').addPassion(passion).build();
      const noMatch = new HotelBuilder().addName('Other').addPassion('pool').build();
      const allDestinations = [cityAndPassionMatch, noMatch, onlyPassionMatches];

      const destinations = findDestination(allDestinations, 'cocktails Darmstadt');

      assertThat(destinations, hasItem(cityAndPassionMatch));
    });
    it('the `name` of a city and the attribute `city` of a hotel', () => {
      const cityName = 'Valencia';
      const hotel = new HotelBuilder().addName('Some').addCity(cityName).addPassion('cocktails').build();
      const city = new CityBuilder().addName(cityName).build();
      const cityWhichWontBeFound = new CityBuilder().addName('Berlin').build();
      const allDestinations = [hotel, city, cityWhichWontBeFound];

      const destinations = findDestination(allDestinations, 'beach Valencia');

      assertThat(destinations, containsInAnyOrder(hotel, city));
    });
  });
});