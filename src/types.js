type DestinationType =
  CountryType |
  RegionType |
  CityType |
  HotelType
;
type CountryType = {
  type: 'country';
  name: string;
};
type RegionType = {
  type: 'region';
  name: string;
};
type CityType = {
  type: 'city';
  name: string;
};
type HotelType = {
  type: 'hotel';
  name: string;
  city: string;
  passions: Array<string>;
};

type DestinationListType =
  Array<DestinationType>;