// @flow
class DestinationBuilder<T: DestinationType> {
  destination: T;
  build() {
    return this.destination;
  }
  addName(name: string): this {
    this.destination.name = name;
    return this;
  }
}
export class HotelBuilder extends DestinationBuilder<HotelType> {
  constructor() {
    super();
    this.destination = {type: 'hotel', name: '', passions: [], city: ''};
  }
  addPassion(passion: string) {
    this.destination.passions.push(passion);
    return this;
  }
  addCity(city: string) {
    this.destination.city = city;
    return this;
  }
}
export class CityBuilder extends DestinationBuilder<CityType> {
  constructor() {
    super();
    this.destination = {type: 'city', name: ''};
  }
}
export class RegionBuilder extends DestinationBuilder<RegionType> {
  constructor() {
    super();
    this.destination = {type: 'region', name: ''};
  }
}
