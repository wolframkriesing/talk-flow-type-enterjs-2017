## Table of Contents

* [Typing JS with Flow](#typing-js-with-flow)
* [The problem to solve](#the-problem-to-solve)
* [Getting started, as a kata](#getting-started-as-a-kata)
* [What's in the repo?](#whats-in-the-repo)
* [Docs](#docs)

----

## Typing JS with Flow

This repo contains all the stuff I prepared and used for [the talk at the enterjs][talk] 
conference about [flow].
The essence of it all, or maybe just the thing I was most passionate about was to show
how to use flow for having [exhaustiveness checks][exhaustiveness] for our code.
Big thanks goes to [Vladimir Kurchatkin][vladi] who has helped me a lot by answering
[various questions][flow-qs] which helped me to get exhaustiveness explained properly
and enabled me to show it in the simplest way, I guess.

The repo contains all the branches as I developed the code for it. See the [explainations] for 
understanding what to find in all the branches.

## The problem to solve

The main thing is to enhance existing (untyped) JS code with type annotations and understand what
value this adds, where and how the tool works.

The problem that the tests solve is basically a simplified version of the auto-suggest of the 
search on the [HolidayCheck website][holidaycheck] which might be very much alike to other websites.
When searching on the HolidayCheck (HC) site you want to find holiday **destinations**. A Destination is the first
type we identify. On this image 
<img src="./docs/the-types.png" alt="the types explained" width="300" style="float: left; margin: 1rem;"/>
one can see the three different types the search might return.

They are: region, city and hotel. In real life the hotel search on the HC website might contain so called
`passions` (things like: sauna, yoga, wintersports). A hotel might also have the property `city`, which is the 
city the hotel is located at. That makes the hotel type different to city and region. Which means the search 
for each type is a bit different. Initially in the pure JS solution this is implemented with code like this:

```js
const searchResult =  destination.city && containsString(destination.city, searchTerm) ||
                      destination.passions && contains(destination.passions, searchTerm) ||
                      containsString(destination.name, searchTerm);
```

It works fine when working without types. But it also bares problems. And by typing the variable `destination`
flow will complain about the code above. And it forces one to push this code to become more specific, depending
on the types. 

## Getting started, as a kata

You got here, means you want to try this out :). Cool.
If you continue, I assume you want to use the not-annotated JS version and learn how to add types
to an untyped code base, using flow. If that's the case, follow the instructions below.

1. `git clone git@gitlab.com:wolframkriesing/talk-flow-type-enterjs-2017.git` clone this repository locally
1. `cd talk-flow-type-enterjs-2017` change into the directory the code was cloned into
1. `npm install` will set up all dependencies
1. `npm test` all tests pass, the code works BUT
1. `npm run flow` will run the type checks and the tests afterwards
   if you have cloned the master branch you will get this result
   
    ```
      src/search.js:20
       20:   (destination: empty)
              ^^^^^^^^^^^ object type. This type is incompatible with
       20:   (destination: empty)
                           ^^^^^ empty
    ```
    
    which is the exhaustiveness check and it fails because in [./src/types.js](./src/types.js) we have added the
    line 2 the `CountryType |` which is not yet implemented in the `search.js`. 
    And that error message is the exhaustiveness check, which tells us that.
1. check out the branch `purejs-solution-#1` and go from there and add types gradually
1. if you don't know how to start or would like to follow some instructions, feel free to either
   1. go through the notes I made [page 1](./docs/notes-page1.jpg) and [page 2](./docs/notes-page2.jpg), 
      and follow the instructions (if you can read them :)) OR
   2. read the commits of the branch [add-typing-incl-exhaustiveness-#3][typed-branch] where I 
      did it in one possible way and ended up with what is in master currently, start at [this commit][typed-commits]

## What's in the repo?

The initial pure JS code can be found in the repo in the [purejs-solution-#1 branch][purejs-branch].
That branch was the base from where I added types on top it, which can be found in [the branch add-typing-incl-exhaustiveness-#3][typed-branch].
If you are interested in every step, check out the commits in chronological order, I tried to commit 
as small as possible and tried to write good commit messages.

## Docs

In the docs folder you may find the slides, my notes for live coding and the already mentioned
explaination for the branches.

[talk]: https://www.enterjs.de/abstracts#flowtype-strikte-java-script-typisierung-in-beliebiger-dosis
[flow]: https://flow.org/
[exhaustiveness]: http://www.adamsolove.com/js/flow/type/2016/04/15/flow-exhaustiveness.html
[vladi]: https://twitter.com/vkurchatkin
[flow-qs]: https://twitter.com/wolframkriesing/status/876744685217349632
[explainations]: ./docs/explaination-of-all-branches.png
[the-types]: ./docs/the-types.png
[holidaycheck]: http://holidaycheck.de
[purejs-branch]: https://gitlab.com/wolframkriesing/talk-flow-type-enterjs-2017/tree/purejs-solution-%231
[typed-branch]: https://gitlab.com/wolframkriesing/talk-flow-type-enterjs-2017/tree/add-typing-incl-exhaustiveness-%233
[typed-commits]: https://gitlab.com/wolframkriesing/talk-flow-type-enterjs-2017/commit/df3d4778b48f53dde0fa5fc53ddd82c37553bb82
